﻿using Gourmy.Models;
using Gourmy.Views;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    class MainMenuViewModel : ViewModel
    {
        public ICommand RecipesPageCommand { get; private set; }

        public ICommand IngredientsPageCommand { get; private set; }

        public MainMenuViewModel()
        {
            RecipesPageCommand = new Command(async () => await GoToRecipesPageAsync());
            IngredientsPageCommand = new Command(async () => await GoToIngredientsPageAsync());
        }

        private async Task GoToRecipesPageAsync()
        {
            //ImagePreloader imagepreload = ImagePreloader.Instance;
            await PageManager.Instance.PushAsync(new RecipesListPage());
        }

        private async Task GoToIngredientsPageAsync()
        {
            await PageManager.Instance.PushAsync(new IngredientsListPage(null));
        }
    }
}
