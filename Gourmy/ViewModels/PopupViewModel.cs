﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    //ViewModel used by all PopupViewModels
    public class PopupViewModel : ViewModel
    {
        public ICommand ClosePopupCommand { get; set; }

        public PopupViewModel()
        {
            ClosePopupCommand = new Command(async () => await ClosePopup());
        }

        //Common command used by PopupViewModels to close themselves
        private async Task ClosePopup()
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}
