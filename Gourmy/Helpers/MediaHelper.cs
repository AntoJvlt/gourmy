﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Gourmy.Models
{
    //Helper to handle media interaction with the Media plugin
    static class MediaHelper
    {
        //Return image picked by the user on his device
        public static async Task<Stream> UserPickImage()
        {
            //Verify that the device support the functionnality
            if (CrossMedia.Current.IsPickPhotoSupported)
            {
                //User can pick file from his device storage
                MediaFile file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                {
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Small,
                    CompressionQuality = 92
                });

                return file?.GetStream();
            }
            else
            {
                await PageManager.Instance.DisplayAlert("Erreur", "Cette fonctionnalité n'est pas supportée sur votre appareil", "ok");
            }

            return null;
        }
    }
}
