﻿using Gourmy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipesListPage : ContentPage
    {
        private RecipesListViewModel _viewModel;

        public RecipesListPage()
        {
            InitializeComponent();

            _viewModel = new RecipesListViewModel();

            BindingContext = _viewModel;
        }

        //Load the data of the ViewModel when the page appears
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await _viewModel.LoadDataAsync();
        }
    }
}