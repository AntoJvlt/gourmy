﻿using Gourmy.Models;
using Gourmy.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;

namespace Gourmy.ViewModels
{
    public class RecipesListViewModel : PageViewModel
    {
        public ICommand AddRecipeCommand { get; set; }

        public ICommand ChangeFavoriteCommand { get; set; }

        public ICommand UpdateListResearch { get; set; }

        public ImageSource Star_image { get; set; }

        public ICommand DisplayRecipe { get; set; }

        private ObservableCollection<Recipe> _recipes = new ObservableCollection<Recipe>();

        private bool _isDataLoaded = false;

        public RecipesListViewModel()
        {
            AddRecipeCommand = new Command(async () => await OpenRecipeEditingPageAsync());
            ChangeFavoriteCommand = new Command<Recipe>(async (recipe) => await ChangeFavorite(recipe));
            DisplayRecipe = new Command<Recipe>(async (recipe) => await DisplayRecipeAsync(recipe));
            UpdateListResearch = new Command<string>(async (research) => await UpdateListBasedOnResearch(research));
            Star_image = ImageSource.FromFile("star_empty_min.png");
        }

        public ObservableCollection<Recipe> Recipes
        {
            get
            {
                return _recipes;
            }
            set
            {
                SetValue<ObservableCollection<Recipe>>(ref _recipes, value);
            }
        }

        //Init all data async when the page appears
        public async Task LoadDataAsync()
        {
            if (_isDataLoaded)
                return;

            _isDataLoaded = true;

            List<Recipe> dbRecipes = (await DBRequester<Recipe>.GetItemsAsync()).OrderBy(r => !r.IsFavorite ? 1 : 0).ToList();

            Recipes = new ObservableCollection<Recipe>(dbRecipes);
        }

        //Auto completion used to search recipes
        private async Task UpdateListBasedOnResearch(string research)
        {
            List<Recipe> allRecipes = await DBRequester<Recipe>.GetItemsAsync();
            List<Recipe> researchList = new List<Recipe>();
            
            //Filter the recipes
            foreach (Recipe recipe in allRecipes)
                if (recipe.Name.ToLower().Contains(research.ToLower()))
                    researchList.Add(recipe);

            //Order base on favorite factor
            List<Recipe> orderedList = researchList.OrderBy(r => !r.IsFavorite ? 1 : 0).ToList();

            Recipes = new ObservableCollection<Recipe>(orderedList);
        }

        private async Task OpenRecipeEditingPageAsync()
        {
            //Use callback to update the edited values dynamically
            await PageManager.Instance.PushAsync(new RecipeEditingPage(RecipeViewModel.CreateSync(new Recipe()), new Action(async () =>
            {
                Recipes.Add((await DBRequester<Recipe>.GetItemsQueryAsync("SELECT * FROM Recipe ORDER BY Id DESC LIMIT 1"))[0]);
            })));
        }

        private async Task ChangeFavorite(Recipe recipe)
        {
            recipe.IsFavorite = !recipe.IsFavorite;

            OrderList();
            await DBRequester<Recipe>.SaveItemAsync(recipe);
        }

        //Order the recipes list base on the favorite factor
        private void OrderList()
        {
            List<Recipe> sortedList = Recipes.OrderBy(r => !r.IsFavorite ? 1 : 0).ToList();

            Recipes.Clear();

            foreach (Recipe recipe in sortedList)
                Recipes.Add(recipe);
        }

        private async Task DisplayRecipeAsync(Recipe recipe)
        {
            //Used callback to know if something changed
            await PageManager.Instance.PushAsync(new RecipeDisplayPage(await RecipeViewModel.CreateAsync(recipe), new Action(async () =>
            {
                //Updated everythings if changed occurs
                List<Recipe> dbRecipes = (await DBRequester<Recipe>.GetItemsAsync()).OrderBy(r => !r.IsFavorite ? 1 : 0).ToList();
                Recipes = new ObservableCollection<Recipe>(dbRecipes);
            })));
        }
    }
}
