﻿using Gourmy.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace Gourmy.Models
{
    //Convert people amount of a recipe to string view
    class NumberToPeopleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int val = (int)value;

            if(val > 1)
                return $"Pour {val} personnes :";
            else
                return $"Pour {val} personne :";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
