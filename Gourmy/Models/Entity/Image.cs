﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gourmy.Models
{
    [Table("Image")]
    class Image : IDBEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
    }
}
