﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gourmy.Models
{
    [Table("RecipeStep")]
    public class RecipeStep : IDBEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int RecipeId { get; set; }

        public int Number { get; set; } = 1;

        public string Content { get; set; }
        public RecipeStep Clone()
        {
            return (RecipeStep)MemberwiseClone();
        }
    }
}
