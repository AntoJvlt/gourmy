﻿using Gourmy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipeEditingPage : ContentPage
    {
        public RecipeEditingPage(RecipeViewModel recipeViewModel, Action doneCallback)
        {
            InitializeComponent();

            RecipeEditingViewModel recipeEditingViewModel = new RecipeEditingViewModel(recipeViewModel, doneCallback);
            BindingContext = recipeEditingViewModel;

            //Use delegate to provide scroll action on view control
            recipeEditingViewModel.ScrollDownAction = new Action(async () =>
            {
                await ScrollControl.ScrollToAsync(0, MainStackLayout.Height, true);
            });
        }
    }
}