﻿using Gourmy.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoryListPopup : PopupPage
    {
        private CategoryListViewModel _categoryListViewModel;

        public CategoryListPopup(IngredientEditingViewModel ingredientEditingViewModel)
        {
            _categoryListViewModel = new CategoryListViewModel(ingredientEditingViewModel);
            BindingContext = _categoryListViewModel;

            InitializeComponent();

        }

        //Load the data of the ViewModel when the page appears
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await _categoryListViewModel.LoadData();
        }
    }
}