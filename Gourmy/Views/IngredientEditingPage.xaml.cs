﻿
using Gourmy.Models;
using Gourmy.ViewModels;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IngredientEditingPage : ContentPage
    {
        public IngredientEditingPage(IngredientViewModel ingredientViewModel, Action<IngredientViewModel, IngredientViewModel> updateIngredientsListAction)
        {
            InitializeComponent();
            BindingContext = new IngredientEditingViewModel(ingredientViewModel, updateIngredientsListAction);
        }
    }
}