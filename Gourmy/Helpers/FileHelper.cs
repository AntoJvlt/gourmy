﻿using Gourmy.Models;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Gourmy.Helpers
{
    //Helper class which help to handle file with PCLStorage
    class FileHelper
    {
        public static async Task CreateFileFromStreamAsync(string filename, Stream fileContentStream, string folderLocation = null)
        {
            IFolder folder = GetRootFolder();

            if (!string.IsNullOrWhiteSpace(folderLocation))
            {
                // create a folder, if one does not exist already, or open it.
                folder = await folder.CreateFolderAsync(folderLocation, CreationCollisionOption.OpenIfExists);
            }

            IFile file = await folder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

            byte[] fileContent = await GetBytesFromStreamAsync(fileContentStream);

            //Write fileContent into new MemoryStream
            using (MemoryStream memoryStreamHandler = new MemoryStream())
            {
                await memoryStreamHandler.WriteAsync(fileContent, 0, fileContent.Length);

                //Copy stream content into stream of the new file just created
                using (Stream fileStreamHandler = await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite))
                {
                    memoryStreamHandler.Position = 0;
                    await memoryStreamHandler.CopyToAsync(fileStreamHandler);
                }
            }
        }

        public static async Task CreatePngFileFromStreamAsync(int imageId, Stream stream)
        {
            await CreateFileFromStreamAsync($"{imageId}.png", stream, "Images");
        }


        public static async Task DeletePngFileFromIdAsync(int imageId)
        {
            IFolder folder = await GetRootFolder().GetFolderAsync("Images");

            //Check existence before delete
            if (await folder.CheckExistsAsync($"{imageId}.png") == ExistenceCheckResult.FileExists)
            {
                IFile file = await folder.GetFileAsync($"{imageId}.png");
                await file.DeleteAsync();
            }
        }

        //Return array of byte containing the bytes of the stream
        public static async Task<byte[]> GetBytesFromStreamAsync(Stream fileContentStream)
        {
            //Copy the stream before consuming it
            MemoryStream stream = await CopyAndResetPositionStreamAsync(fileContentStream);

            return stream.ToArray();
        }

        //Copy a stream, usefull for futur consumption
        public async static Task<MemoryStream> CopyAndResetPositionStreamAsync(Stream stream)
        {   
            MemoryStream copyStream = new MemoryStream();
            await stream.CopyToAsync(copyStream);
            stream.Seek(0, SeekOrigin.Begin); //Reset the position to the beggining of the stream
            copyStream.Seek(0, SeekOrigin.Begin);

            return copyStream;
        }

        //Use PCLStorage to get the root folder of the internal storage
        public static IFolder GetRootFolder()
        {
            return FileSystem.Current.LocalStorage;
        }

        //Return the image file path inside the internal storage
        public static string GetImageFilePathById(int id)
        {
            return Constants.AppFolderPath + "/Images" + $"/{id}.png";
        }

}
}
