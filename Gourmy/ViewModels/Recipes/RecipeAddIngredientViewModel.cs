﻿using Gourmy.Models;
using Gourmy.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    public class RecipeAddIngredientViewModel : PageViewModel
    {
        public ICommand ChooseIngredientCommand { get; set; }

        public ICommand ValidateCommand { get; set; }

        public RecipeIngredient RecipeIngredient { get; set; } = new RecipeIngredient();

        private IngredientViewModel _ingredientViewModel;

        private bool _ingredientVisible = false;

        public RecipeAddIngredientViewModel(Action<RecipeIngredientViewModel> onValidateAction)
        {
            ChooseIngredientCommand = new Command(async () => await OpenIngredientListAsync());
            ValidateCommand = new Command(async () => await Validate(onValidateAction));
        }

        public IngredientViewModel IngredientViewModel
        {
            get
            {
                return _ingredientViewModel;
            }
            set
            {
                SetValue<IngredientViewModel>(ref _ingredientViewModel, value);
            }
        }

        public bool IngredientVisible
        {
            get
            {
                return _ingredientVisible;
            }
            set
            {
                SetValue<bool>(ref _ingredientVisible, value);
            }
        }

        private async Task OpenIngredientListAsync()
        {
            //Callback is used to get the ingredientViewModel chosen by the user
            await PageManager.Instance.PushAsync(new IngredientsListPage(new Action<IngredientViewModel>(async (ingredientViewModel) =>
            {
                IngredientViewModel = ingredientViewModel;
                IngredientVisible = true;
                await PageManager.Instance.PopAsync();
            })));
        }

        private async Task Validate(Action<RecipeIngredientViewModel> onValidateAction)
        {
            //Ingredient has to be set by the user in order to save it
            if(IngredientViewModel != null)
            {
                RecipeIngredient.IngredientId = IngredientViewModel.Ingredient.Id;
                onValidateAction.Invoke(new RecipeIngredientViewModel(IngredientViewModel, RecipeIngredient));
                await PageManager.Instance.PopAsync();
            }
            else
            {
                await PageManager.Instance.DisplayAlert("Erreur", "Aucun ingrédient sélectionné", "ok");
            }
        }
    }
}
