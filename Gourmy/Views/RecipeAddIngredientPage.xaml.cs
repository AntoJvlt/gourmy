﻿using Gourmy.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipeAddIngredientPage : ContentPage
    {
        public RecipeAddIngredientPage(Action<RecipeIngredientViewModel> onValidateAction)
        {
            InitializeComponent();

            BindingContext = new RecipeAddIngredientViewModel(onValidateAction);
        }
    }
}