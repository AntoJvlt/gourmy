﻿using Gourmy.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalculationPopup : PopupPage
    {
        public CalculationPopup(ObservableCollection<RecipeIngredientViewModel> ingredients, int basePeopleAmount)
        {
            InitializeComponent();

            CalculationViewModel calculationViewModel = new CalculationViewModel(ingredients, basePeopleAmount);
            BindingContext = calculationViewModel;

            PeopleEntry.Text = $"{calculationViewModel.CurrentPeopleAmount}";
        }
    }
}