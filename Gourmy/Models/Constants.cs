﻿using PCLStorage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Gourmy.Models
{
    class Constants
    {        
        public const string DatabaseFileName = "GourmyDB.db3";

        public const SQLite.SQLiteOpenFlags Flags =
        // open the database in read/write mode
        SQLite.SQLiteOpenFlags.ReadWrite |
        // create the database if it doesn't exist
        SQLite.SQLiteOpenFlags.Create;

        //Folder path of the internal storage
        public static string AppFolderPath
        {
            get
            {
                return FileSystem.Current.LocalStorage.Path;
            }
        }

        public static string DatabasePath
        {
            get
            {
                return Path.Combine(AppFolderPath, DatabaseFileName);
            }
        }
    }
}
