﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gourmy.Models
{
    //Singleton class handling the database connection
    class SQLiteDB
    {
        private static SQLiteAsyncConnection _database;

        private SQLiteDB()
        {
        }

        public static SQLiteAsyncConnection Database
        {
            get
            {
                if(_database == null)
                    _database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);

                return _database;
            }
        }

    }
}
