﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gourmy.Models
{
    
    /*Generic Helper class used to query the database easily
      On each methode, we check if the table exist and create it if it doesnt*/
    static class DBRequester <T> where T : IDBEntity, new()
    {
        //Return all items of a table
        public async static Task<List<T>> GetItemsAsync()
        {
            await SQLiteDB.Database.CreateTableAsync<T>();
            return await SQLiteDB.Database.Table<T>().ToListAsync();
        }

        //Return table items list based on a query
        public async static Task<List<T>> GetItemsQueryAsync(string query, params object[] args)
        {
            await SQLiteDB.Database.CreateTableAsync<T>();
            return await SQLiteDB.Database.QueryAsync<T>(query, args);
        }

        //Return one table item based on his id
        public async static Task<T> GetItemAsync(int id)
        {
            await SQLiteDB.Database.CreateTableAsync<T>();
            return await SQLiteDB.Database.Table<T>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        //Save or update table item
        public async static Task<int> SaveItemAsync(T item)
        {
            await SQLiteDB.Database.CreateTableAsync<T>();

            //If Id is already set we update the row, if not we create the entry
            if (item.Id != 0)
                return await SQLiteDB.Database.UpdateAsync(item);
            else
                return await SQLiteDB.Database.InsertAsync(item);
        }

        //Delete table item
        public async static Task<int> DeleteItemAsync(T item)
        {
            await SQLiteDB.Database.CreateTableAsync<T>();
            return await SQLiteDB.Database.DeleteAsync(item);
        }
    }
}
