﻿using Gourmy.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    //Class used to display an Ingredient in a view
    public class IngredientViewModel
    {
        public Ingredient Ingredient { get; set; }

        public string Name { get; set; }

        public string CategoryName { get; set; }

        public int ImageId { get; set; }

        private IngredientViewModel(Ingredient ingredient)
        {
            Ingredient = ingredient;
            Name = ingredient.Name;
            ImageId = ingredient.ImageId;
        }

        //This object need to be created asynchroniously
        public static Task<IngredientViewModel> CreateAsync(Ingredient ingredient)
        {
            IngredientViewModel instance = new IngredientViewModel(ingredient);

            return instance.LoadDataAsync();
        }

        //Load the data async and return the instance
        private async Task<IngredientViewModel> LoadDataAsync()
        {
            if (Ingredient.CategoryId != 0)
            {
                Category category = await DBRequester<Category>.GetItemAsync(Ingredient.CategoryId);

                CategoryName = category.Name;
            }else
            {
                CategoryName = "Aucune";
            }

            return this;
        }

    }
}
