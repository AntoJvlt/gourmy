﻿using Gourmy.Models;
using PCLStorage;
using SQLite;
using System;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainMenuPage mainMenuPage = new MainMenuPage();
            NavigationPage.SetHasNavigationBar(mainMenuPage, false);

            MainPage = new NavigationPage(mainMenuPage);
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
