﻿using Gourmy.Models;
using Gourmy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipeDisplayPage : ContentPage
    {
        public RecipeDisplayPage(RecipeViewModel recipeViewModel, Action modifiedCallback)
        {
            InitializeComponent();

            BindingContext = new RecipeDisplayViewModel(recipeViewModel, modifiedCallback);
        }
    }
}