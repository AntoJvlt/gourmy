﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gourmy.Models
{
    [Table("RecipeIngredient")]
    public class RecipeIngredient : IDBEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int RecipeId { get; set; }

        public int IngredientId { get; set; }

        public int Quantity { get; set; }

        [MaxLength(255)]
        public string QuantityUnit { get; set; }
    }
}
