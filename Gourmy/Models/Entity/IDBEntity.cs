﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gourmy.Models
{
    interface IDBEntity
    {
        int Id { get; set; }
    }
}
