﻿using Gourmy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gourmy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IngredientsListPage : ContentPage
    {
        private IngredientsListViewModel _ingredientsListViewModel;

        public IngredientsListPage(Action<IngredientViewModel> onSelectAction)
        {
            _ingredientsListViewModel = new IngredientsListViewModel(onSelectAction);
            BindingContext = _ingredientsListViewModel;

            InitializeComponent();

        }

        //Load the data of the ViewModel when the page appears
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await _ingredientsListViewModel.LoadDataAsync();
        }
    }
}