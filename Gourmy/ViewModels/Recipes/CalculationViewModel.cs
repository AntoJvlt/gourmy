﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    //Used to calculate the proportions for a recipe
    class CalculationViewModel : PopupViewModel
    {
        public List<RecipeIngredientViewModel> BaseIngredients { get; set; }

        public ICommand UpdateIngredientsValuesCommand { get; set; }

        public int CurrentPeopleAmount { get; set; }

        private int _basePeopleAmount { get; set; }

        private ObservableCollection<RecipeIngredientViewModel> _currentIngredients;

        public CalculationViewModel(ObservableCollection<RecipeIngredientViewModel> ingredients, int basePeopleAmount)
        {
            BaseIngredients = new List<RecipeIngredientViewModel>(ingredients);
            _currentIngredients = new ObservableCollection<RecipeIngredientViewModel>();

            //We need to clone the object to save the default value
            foreach (RecipeIngredientViewModel ingredient in BaseIngredients)
                _currentIngredients.Add(ingredient.Clone());

            _basePeopleAmount = basePeopleAmount;
            CurrentPeopleAmount = basePeopleAmount;
            UpdateIngredientsValuesCommand = new Command<string>((entry) => UpdateIngredientsValues(entry));
        }

        public ObservableCollection<RecipeIngredientViewModel> CurrentIngredients
        {
            get
            {
                return _currentIngredients;
            }
            set
            {
                SetValue<ObservableCollection<RecipeIngredientViewModel>>(ref _currentIngredients, value);
            }
        }

        //Calculate the cross product and update the displayed values
        private void UpdateIngredientsValues(string entry)
        {
            int value;

            if(int.TryParse(entry, out value))
            {
                foreach (RecipeIngredientViewModel ingredient in CurrentIngredients)
                {
                    RecipeIngredientViewModel baseIngredient = null;

                    foreach (RecipeIngredientViewModel baseIngredientViewModel in BaseIngredients)
                    {
                        if (ingredient.RecipeIngredient.Id == baseIngredientViewModel.RecipeIngredient.Id)
                        {
                            baseIngredient = baseIngredientViewModel;
                            break;
                        }
                    }

                    if (baseIngredient != null)
                    {
                        ingredient.Quantity = (value * baseIngredient.Quantity) / _basePeopleAmount;
                        CurrentIngredients = new ObservableCollection<RecipeIngredientViewModel>(CurrentIngredients);
                    }
                }
            }
        }
    }
}
