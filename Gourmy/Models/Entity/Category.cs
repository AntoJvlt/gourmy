﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gourmy.Models
{
    [Table("Category")]
    public class Category : IDBEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
            
        [MaxLength(255), Unique]
        public string Name { get; set; }
    }
}
