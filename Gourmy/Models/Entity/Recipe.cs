﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gourmy.Models
{
    [Table("Recipe")]
    public class Recipe : IDBEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        public int ImageId { get; set; }

        public int AmountPeople { get; set; }

        public bool IsFavorite { get; set; } = false;
    }
}
