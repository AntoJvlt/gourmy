﻿using Gourmy.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    class EntryResearcherBehavior : Behavior<Entry>
    {
        public static readonly BindableProperty CommandProperty =
           BindableProperty.Create("Command", typeof(ICommand), typeof(EntryResearcherBehavior), null);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public Entry AssociatedObject { get; private set; }

        protected override void OnAttachedTo(Entry entry)
        {
            entry.TextChanged += OnEntryTextChanged;
            AssociatedObject = entry;
            entry.BindingContextChanged += OnBindingContextChanged;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.TextChanged -= OnEntryTextChanged;
            entry.BindingContextChanged -= OnBindingContextChanged;
            AssociatedObject = null;
            base.OnDetachingFrom(entry);
        }

        void OnBindingContextChanged(object sender, EventArgs e)
        {
            OnBindingContextChanged();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            BindingContext = AssociatedObject.BindingContext;
        }

        void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            Command.Execute(((Entry)sender).Text);
        }
    }
}
