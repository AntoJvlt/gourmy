﻿using Gourmy.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace Gourmy.Models
{
    /*Convert ImageId to ImageSource
      Accept parameter defining the default image*/
    class IdToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ImageSource imageSource;

            if ((int)value != 0)
                imageSource = ImageSource.FromFile(FileHelper.GetImageFilePathById((int)value));
            else
                imageSource = ImageSource.FromFile((string)parameter);

            return imageSource;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
