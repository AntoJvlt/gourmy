﻿


using Gourmy.Models;
using Gourmy.Views;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Gourmy.Helpers;

namespace Gourmy.ViewModels
{
    public class RecipeEditingViewModel : PageViewModel
    {
        public RecipeViewModel RecipeViewModel { get; set; }

        public Action ScrollDownAction { get; set; }

        public ICommand PickImageCommand { get; set; }

        public ICommand AddIngredientCommand { get; set; }

        public ICommand DeleteIngredientCommand { get; set; }

        public ICommand AddStepCommand { get; set; }

        public ICommand DeleteStepCommand { get; set; }

        public ICommand DowngradeStepNumberCommand { get; set; }

        public ICommand UpgradeStepNumberCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        public string PageTitle { get; set; }

        public string Name { get; set; }

        public int AmountPeople { get; set; }

        private Stream _imageStream;

        private ImageSource _imageSource;

        private ObservableCollection<RecipeIngredientViewModel> _ingredients;

        private ObservableCollection<RecipeStep> _steps;

        private bool _isSaving = false;

        public RecipeEditingViewModel(RecipeViewModel recipeViewModel, Action doneCallback)
        {
            RecipeViewModel = recipeViewModel;

            if(RecipeViewModel.Recipe.Id != 0)
            {
                PageTitle = "Modification";
                Name = recipeViewModel.Recipe.Name;
                AmountPeople = recipeViewModel.Recipe.AmountPeople;

                Ingredients = new ObservableCollection<RecipeIngredientViewModel>();
                Steps = new ObservableCollection<RecipeStep>();

                //Lists need to be clone in order to not alter the current value of the objects in others page
                foreach (RecipeIngredientViewModel recipeIngredientViewModel in recipeViewModel.IngredientsList)
                    Ingredients.Add(recipeIngredientViewModel.Clone());

                foreach (RecipeStep recipeStep in recipeViewModel.StepsList)
                    Steps.Add(recipeStep.Clone());

                //Sort the step list
                SortAndUpdateStepsList();

                if (RecipeViewModel.Recipe.ImageId != 0)
                    ImageSource = ImageSource.FromFile(FileHelper.GetImageFilePathById(RecipeViewModel.Recipe.ImageId));
            }else
            {
                PageTitle = "Création";
                Ingredients = new ObservableCollection<RecipeIngredientViewModel>();
                _steps = new ObservableCollection<RecipeStep>();
            }

            if (RecipeViewModel.Recipe.Id == 0 || RecipeViewModel.Recipe.ImageId == 0)
                ImageSource = ImageSource.FromFile("recipe_default_min.png");

            PickImageCommand = new Command(async () => await PickImage());
            AddIngredientCommand = new Command(async () => await OpenAddIngredientPopup());
            DeleteIngredientCommand = new Command<RecipeIngredientViewModel>((recipeIngredientViewModel) => DeleteIngredient(recipeIngredientViewModel));
            AddStepCommand = new Command(() => AddStep());
            DeleteStepCommand = new Command<RecipeStep>((recipeStep) => DeleteStep(recipeStep));
            DowngradeStepNumberCommand = new Command<RecipeStep>((recipeStep) => DowngradeStepNumber(recipeStep));
            UpgradeStepNumberCommand = new Command<RecipeStep>((recipeStep) => UpdateStepNumber(recipeStep));
            SaveCommand = new Command(async (callback) => await SaveAsync(doneCallback));
        }

        public ObservableCollection<RecipeIngredientViewModel> Ingredients
        {
            get
            {
                return _ingredients;
            }
            set
            {
                SetValue<ObservableCollection<RecipeIngredientViewModel>>(ref _ingredients, value);
            }
        }

        public ObservableCollection<RecipeStep> Steps
        {
            get
            {
                return _steps;
            }
            set
            {
                SetValue<ObservableCollection<RecipeStep>>(ref _steps, value);
            }
        }

        public ImageSource ImageSource
        {
            get
            {
                return _imageSource;
            }
            set
            {
                SetValue(ref _imageSource, value);
            }
        }

        private async Task PickImage()
        {
            if (!_isSaving)
            {
                _imageStream = await MediaHelper.UserPickImage();

                //Update image source if user picked an image
                if (_imageStream != null)
                {
                    MemoryStream copyStream = await FileHelper.CopyAndResetPositionStreamAsync(_imageStream);

                    ImageSource = ImageSource.FromStream(() =>
                    {
                        return copyStream;
                    });
                }
            }
        }

        private void UpdateStepNumber(RecipeStep recipeStep)
        {
            if(!_isSaving)
            {
                if (recipeStep.Number < Steps.Count)
                {
                    //Reduce by 1 the step number of the step which was at the new position of the updated step.
                    foreach (RecipeStep step in Steps)
                    {
                        if (step.Number == recipeStep.Number + 1)
                        {
                            step.Number -= 1;
                            break;
                        }
                    }

                    recipeStep.Number += 1;

                    SortAndUpdateStepsList();
                }
            }
        }

        private void DowngradeStepNumber(RecipeStep recipeStep)
        {
            if (!_isSaving)
            {
                if (recipeStep.Number - 2 >= 0)
                {
                    //Increase by 1 the step number of the step which was at the new position of the updated step.
                    foreach (RecipeStep step in Steps)
                    {
                        if (step.Number == recipeStep.Number - 1)
                        {
                            step.Number += 1;
                            break;
                        }
                    }

                    recipeStep.Number -= 1;

                    SortAndUpdateStepsList();
                }
            }
        }

        private void SortAndUpdateStepsList()
        {
            List<RecipeStep> sortedList = Steps.OrderBy(i => i.Number).ToList();
            Steps = new ObservableCollection<RecipeStep>(sortedList);
        }

        private void AddStep()
        {
            if (!_isSaving)
            {
                Steps.Add(new RecipeStep()
                {
                    Number = _steps.Count + 1
                });

                //Scroll down to the page
                ScrollDownAction.Invoke();
            }
        }

        private void DeleteStep(RecipeStep recipeStep)
        {
            if (!_isSaving)
            {
                IEnumerable<RecipeStep> upperSteps = from step in Steps where step.Number > recipeStep.Number select step;

                foreach (RecipeStep step in upperSteps)
                    step.Number -= 1;

                Steps.Remove(recipeStep);

                SortAndUpdateStepsList();
            }
        }

        private async Task OpenAddIngredientPopup()
        {
            if (!_isSaving)
            {
                //Used callback to get the added ingredient
                await PageManager.Instance.PushAsync(new RecipeAddIngredientPage(new Action<RecipeIngredientViewModel>((recipeIngredientViewModel) =>
                {
                    Ingredients.Add(recipeIngredientViewModel);
                })));
            }
        }

        private void DeleteIngredient(RecipeIngredientViewModel ingredient)
        {
            if (!_isSaving)
            {
                Ingredients.Remove(ingredient);
            }
        }

        private async Task SaveAsync(Action doneCallback)
        {
            _isSaving = true;

            if (String.IsNullOrEmpty(Name))
            {
                await PageManager.Instance.DisplayAlert("Erreur", "La recette n'a pas de nom", "ok");
                return;
            }

            if (AmountPeople == 0)
            {
                await PageManager.Instance.DisplayAlert("Erreur", "La quantité de personnes ne peut pas être de 0", "ok");
                return;
            }

            if ((RecipeViewModel.Recipe.Id != 0 && RecipeViewModel.Recipe.Name == Name) || (await DBRequester<Recipe>.GetItemsQueryAsync("SELECT * FROM Recipe WHERE Name = ?", Name)).Count == 0)
            {
                int recipeId =  await SavedRecipeIfChangedAsync();
                await SaveRecipeIngredientsAsync(recipeId);
                await SaveRecipeStepsAsync(recipeId);

                doneCallback.Invoke();
                await PageManager.Instance.PopAsync();
            }
            else
            {
                await PageManager.Instance.DisplayAlert("Erreur", $"La recette {Name} existe déjà.", "ok");
            }
        }

        private async Task<int> SavedRecipeIfChangedAsync()
        {
            int idReturned = RecipeViewModel.Recipe.Id;


            //Check if Recipe values changed
            if (_imageStream != null || Name != RecipeViewModel.Recipe.Name || AmountPeople != RecipeViewModel.Recipe.AmountPeople)
            {
                Recipe recipe = new Recipe()
                {
                    Name = Name,
                    AmountPeople = AmountPeople,
                    ImageId = RecipeViewModel.Recipe.ImageId         
                };

                //Save image if not null
                if (_imageStream != null)
                {
                    if (RecipeViewModel.Recipe.ImageId != 0)
                    {
                        //Remove old image
                        await DBRequester<Models.Image>.DeleteItemAsync(await DBRequester<Models.Image>.GetItemAsync(RecipeViewModel.Recipe.ImageId));
                        await FileHelper.DeletePngFileFromIdAsync(RecipeViewModel.Recipe.ImageId);
                    }

                    //Create image
                    await DBRequester<Models.Image>.SaveItemAsync(new Models.Image());
                    recipe.ImageId = (await DBRequester<Models.Image>.GetItemsAsync()).Last().Id;

                    await FileHelper.CreatePngFileFromStreamAsync(recipe.ImageId, _imageStream);
                }

                recipe.Id = idReturned;

                await DBRequester<Recipe>.SaveItemAsync(recipe);

                if (idReturned == 0)
                    idReturned = (await DBRequester<Recipe>.GetItemsQueryAsync("SELECT * FROM Recipe ORDER BY Id DESC LIMIT 1"))[0].Id;
            }

            return idReturned;
        }

        private async Task SaveRecipeIngredientsAsync(int recipeId)
        {
            List<RecipeIngredientViewModel> oldValues = RecipeViewModel.IngredientsList;

            foreach(RecipeIngredientViewModel oldIngredient in oldValues)
            {
                bool hasBeenRemoved = true;

                //Check if user removed this ingredient
                foreach (RecipeIngredientViewModel currentIngredient in Ingredients)
                {
                    if (currentIngredient.RecipeIngredient.Id == oldIngredient.RecipeIngredient.Id)
                    {
                        hasBeenRemoved = false;
                        break;
                    }
                }

                if (hasBeenRemoved)
                    await DBRequester<RecipeIngredient>.DeleteItemAsync(oldIngredient.RecipeIngredient);
            }

            foreach (RecipeIngredientViewModel currentIngredient in Ingredients)
            {
                bool hasBeenAdded = true;

                //Check if user added this ingredient
                foreach (RecipeIngredientViewModel oldIngredient in oldValues)
                {
                    if (currentIngredient.RecipeIngredient.Id == oldIngredient.RecipeIngredient.Id)
                    {
                        hasBeenAdded = false;
                        break;
                    }
                }

                //User added this ingredient
                if (hasBeenAdded)
                {
                    currentIngredient.RecipeIngredient.RecipeId = recipeId;
                    await DBRequester<RecipeIngredient>.SaveItemAsync(currentIngredient.RecipeIngredient);
                }
            }
        }

        private async Task SaveRecipeStepsAsync(int recipeId)
        {
            List<RecipeStep> oldValues = RecipeViewModel.StepsList;

            foreach (RecipeStep oldRecipeStep in oldValues)
            {
                bool hasBeenRemoved = true;

                //Check if user removed this step
                foreach (RecipeStep currentStep in Steps)
                {
                    if (currentStep.Id == oldRecipeStep.Id)
                    {
                        hasBeenRemoved = false;
                        break;
                    }
                }

                if (hasBeenRemoved)
                    await DBRequester<RecipeStep>.DeleteItemAsync(oldRecipeStep);
            }

            foreach (RecipeStep recipeStep in Steps)
            {
                RecipeStep oldValue = oldValues.Find(step => step.Id == recipeStep.Id);

                //User added or modified this step
                if (oldValue == null || (oldValue.Content != recipeStep.Content || oldValue.Number != recipeStep.Number))
                {
                    if (recipeStep.RecipeId == 0)
                        recipeStep.RecipeId = recipeId;

                    await DBRequester<RecipeStep>.SaveItemAsync(recipeStep);
                }
            }
        }
    }
}
