﻿using Gourmy.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    class CategoryListViewModel : PopupViewModel
    {
        private IngredientEditingViewModel _ingredientEditingViewModel;

        public ICommand SaveCommand { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        public ICommand ChooseCommand { get; private set; }

        public string NewCategoryName { get; set; }

        private ObservableCollection<Category> _categories;

        public Category SelectedCategory { get; set; }

        private bool _isDataLoaded = false;

        public CategoryListViewModel(IngredientEditingViewModel ingredientEditingViewModel)
        {
            _ingredientEditingViewModel = ingredientEditingViewModel;
            SaveCommand = new Command(async () => await SaveCategory());
            DeleteCommand = new Command<Category>(async (category) => await DeleteCategory(category));
            ChooseCommand = new Command(() => FinishCategorySelection());
        }

        public ObservableCollection<Category> Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                SetValue<ObservableCollection<Category>>(ref _categories, value);
            }
        }

        //Load data when the page appears
        public async Task LoadData()
        {
            if (_isDataLoaded)
                return;

            _isDataLoaded = true;

            List<Category> dbCategories = await DBRequester<Category>.GetItemsAsync();
            Categories = new ObservableCollection<Category>(dbCategories);
        }

        private async Task SaveCategory()
        {
            //Check that the category doesnt exist
            if ((await DBRequester<Category>.GetItemsQueryAsync("SELECT * FROM Category WHERE Name = ?", NewCategoryName)).Count == 0)
            {
                Category newCategory = new Category() { Name = NewCategoryName };
                await DBRequester<Category>.SaveItemAsync(newCategory);
                Categories.Add(newCategory);
            }
            else
            {
                await PageManager.Instance.DisplayAlert("Erreur", $"La catégorie {NewCategoryName} existe déjà.", "ok");
            }
        }

        private async Task DeleteCategory(Category category)
        {
            //Check that the category is not used by at least one ingredient
            if ((await DBRequester<Ingredient>.GetItemsQueryAsync("SELECT * FROM Ingredient WHERE CategoryId = ?", category.Id)).Count == 0)
            {
                await DBRequester<Category>.DeleteItemAsync(category);
                Categories.Remove(category);
            }
            else
            {
                await PageManager.Instance.DisplayAlert("Erreur", $"Cette catégorie est utilisée par au moins un ingrédient.", "ok");
            }
        }

        //Execute when user select one category
        private void FinishCategorySelection()
        {
            _ingredientEditingViewModel.SetCategory(SelectedCategory);
            ClosePopupCommand.Execute(null);
        }
    }
}
