﻿using Gourmy.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gourmy.ViewModels
{
    //Class used to display a recipe in a view
    public class RecipeViewModel
    {
        public Recipe Recipe { get; set; }

        public List<RecipeIngredientViewModel> IngredientsList { get; set; } = new List<RecipeIngredientViewModel>();

        public List<RecipeStep> StepsList { get; set; } = new List<RecipeStep>();

        private RecipeViewModel(Recipe recipe)
        {
            Recipe = recipe;
        }

        public static RecipeViewModel CreateSync(Recipe recipe)
        {
            return new RecipeViewModel(recipe);
        }

        //class need to be created async
        public static Task<RecipeViewModel> CreateAsync(Recipe recipe)
        {
            RecipeViewModel instance = new RecipeViewModel(recipe);

            return instance.LoadDataAsync();
        }

        //Date need to be load async when the user want to instantiate the class
        private async Task<RecipeViewModel> LoadDataAsync()
        {
            List<RecipeIngredient> recipeIngredientsList = await DBRequester<RecipeIngredient>.GetItemsQueryAsync("SELECT * FROM RecipeIngredient WHERE RecipeId = ?", Recipe.Id);

           foreach(RecipeIngredient recipeIngredient in recipeIngredientsList)
           {
                Ingredient ingredient = await DBRequester<Ingredient>.GetItemAsync(recipeIngredient.IngredientId);
                IngredientViewModel ingredientViewModel = await IngredientViewModel.CreateAsync(ingredient);

                IngredientsList.Add(new RecipeIngredientViewModel(ingredientViewModel, recipeIngredient));
           }

            StepsList = await DBRequester<RecipeStep>.GetItemsQueryAsync("SELECT * FROM RecipeStep WHERE RecipeId = ?", Recipe.Id);

            return this;
        }

    }
}
