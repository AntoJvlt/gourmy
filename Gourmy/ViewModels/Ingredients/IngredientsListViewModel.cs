﻿using Gourmy.Models;
using Gourmy.Views;
using PCLStorage;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    class IngredientsListViewModel : PageViewModel
    {
        public ICommand CreateIngredientCommand { get; set; }

        public ICommand ChooseCommand { get; set; }

        public ICommand DeleteCommand { get; set; }

        public ICommand EditCommand { get; set; }

        public ICommand UpdateListResearch { get; set; }

        public IngredientViewModel SelectedIngredient { get; set; }

        private ObservableCollection<IngredientViewModel> _ingredients = new ObservableCollection<IngredientViewModel>();

        private bool _isDataLoaded = false;

        public IngredientsListViewModel(Action<IngredientViewModel> onSelectAction)
        {
            CreateIngredientCommand = new Command(async () => await OpenIngredientEditingPopupAsync(await IngredientViewModel.CreateAsync(new Ingredient())));
            ChooseCommand = new Command(() => ExecuteChoose(onSelectAction));
            DeleteCommand = new Command<IngredientViewModel>(async (ingredientViewModel) => await DeleteIngredientAsync(ingredientViewModel));
            EditCommand = new Command<IngredientViewModel>(async (ingredientViewModel) => await OpenIngredientEditingPopupAsync(ingredientViewModel));
            UpdateListResearch = new Command<string>(async (research) => await UpdateListBasedOnResearch(research));
        }

        public ObservableCollection<IngredientViewModel> Ingredients
        {
            get
            {
                return _ingredients;
            }
            set
            {
                SetValue<ObservableCollection<IngredientViewModel>>(ref _ingredients, value);
            }
        }

        //Init all the data when the page appears
        public async Task LoadDataAsync()
        {
            if (_isDataLoaded)
                return;

            _isDataLoaded = true;

            List<Ingredient> dbIngredients = await DBRequester<Ingredient>.GetItemsAsync();

            foreach (Ingredient ingredient in dbIngredients)
                Ingredients.Add(await IngredientViewModel.CreateAsync(ingredient));
        }

        private async Task UpdateListBasedOnResearch(string research)
        {
            List<Ingredient> allIngredients = await DBRequester<Ingredient>.GetItemsAsync();

            Ingredients.Clear();

            foreach (Ingredient ingredient in allIngredients)
                if (ingredient.Name.ToLower().Contains(research.ToLower()))
                    Ingredients.Add(await IngredientViewModel.CreateAsync(ingredient));
        }

        public void ExecuteChoose(Action<IngredientViewModel> onSelectAction)
        {
            if(SelectedIngredient != null && onSelectAction != null)
                onSelectAction.Invoke(SelectedIngredient);
        }

        private async Task DeleteIngredientAsync(IngredientViewModel ingredientViewModel)
        {
            //Check that the ingredient is not used by a recipe
            if ((await DBRequester<RecipeIngredient>.GetItemsQueryAsync("SELECT * FROM RecipeIngredient WHERE IngredientId = ?", ingredientViewModel.Ingredient.Id)).Count == 0)
            {
                //Ask is user is sure
                if (await PageManager.Instance.DisplayAlert("Erreur", $"Voulez-vous vraiment supprimer cet ingrédient ?", "Oui", "Non"))
                {
                    //Delete image if set
                    if (ingredientViewModel.ImageId != 0)
                    {
                        Models.Image image = await DBRequester<Models.Image>.GetItemAsync(ingredientViewModel.ImageId);
                        await DBRequester<Models.Image>.DeleteItemAsync(image);
                    }

                    await DBRequester<Ingredient>.DeleteItemAsync(ingredientViewModel.Ingredient);

                    Ingredients.Remove(ingredientViewModel);
                }
            }
            else
            {
                await PageManager.Instance.DisplayAlert("Erreur", $"Cet ingrédient est utilisée par au moins une recette.", "ok");
            }
        }

        private async Task OpenIngredientEditingPopupAsync(IngredientViewModel ingredientViewModel)
        {
            //Callback is used to update this page dynamically
            await PageManager.Instance.PushAsync(new IngredientEditingPage(ingredientViewModel, new Action<IngredientViewModel, IngredientViewModel>((oldIngredientViewModel, newIngredientViewModel) =>
            {
                Ingredients.Remove(oldIngredientViewModel);
                Ingredients.Add(newIngredientViewModel);
            })));
        }
    }
}
