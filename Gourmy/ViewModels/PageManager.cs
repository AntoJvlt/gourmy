﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Gourmy
{
    //Singleton used to hangle the page tree of the app
    public sealed class PageManager
    {
        private static PageManager instance = null;

        public static PageManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new PageManager();

                return instance;
            }
        }

        private Page MainPage
        {
            get { return Application.Current.MainPage; }
        }

        private PageManager() { }

        //Display information alert on the current displayedPage 
        public async Task DisplayAlert(string title, string message, string ok)
        {
            await MainPage.DisplayAlert(title, message, ok);
        }

        //Display question alert on the current displayedPage 
        public async Task<bool> DisplayAlert(string title, string message, string ok, string cancel)
        {
            return await MainPage.DisplayAlert(title, message, ok, cancel);
        }

        //Push the page on the top of the tree
        public async Task PushAsync(Page page)
        {
            NavigationPage.SetHasNavigationBar(page, false);
            await MainPage.Navigation.PushAsync(page, true);
        }

        //Delete the current page of the tree
        public async Task<Page> PopAsync()
        {
            return await MainPage.Navigation.PopAsync(true);
        }
    }
}
