﻿using Gourmy.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace Gourmy.Models
{
    //Convert bool value to differents ImageSources based on the favorite factor of a recipe
    class FavoriteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isFavorite = (bool)value;
            
            if (isFavorite)
                return ImageSource.FromFile("star_full_min.png");
            else
                return ImageSource.FromFile("star_empty_min.png");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
