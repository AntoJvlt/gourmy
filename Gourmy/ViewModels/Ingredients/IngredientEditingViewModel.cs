﻿using Gourmy.Models;
using Gourmy.Views;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using Gourmy.Helpers;
using System.Reflection;

namespace Gourmy.ViewModels
{
    public class IngredientEditingViewModel : PageViewModel
    {
        public IngredientViewModel IngredientViewModel { get; set; }

        public Ingredient Ingredient { get; set; }

        public ICommand OpenCategoryListCommand { get; set; }

        public ICommand PickImageCommand { get; set; }

        public ICommand CreateOrEditIngredientCommand { get; set; }

        public Action<IngredientViewModel, IngredientViewModel> UpdateIngredientsListAction { get; set; }

        public string ValidationButtonLabel { get; set; }

        public string TitleLabel { get; set; }

        private string _categoryLabel;

        private Stream _imageStream;

        private ImageSource _imageSource;

        public IngredientEditingViewModel(IngredientViewModel ingredientViewModel, Action<IngredientViewModel, IngredientViewModel> updateIngredientsListAction)
        {
            IngredientViewModel = ingredientViewModel;

            Ingredient = new Ingredient()
            {
                Id = IngredientViewModel.Ingredient.Id,
                Name = IngredientViewModel.Name,
                CategoryId = IngredientViewModel.Ingredient.CategoryId,
                ImageId = IngredientViewModel.ImageId
            };

            CategoryLabel = IngredientViewModel.CategoryName;
            UpdateIngredientsListAction = updateIngredientsListAction;

            //Set ImageSource displayed based on the context (edit or create)
            if(Ingredient.ImageId == 0)
                ImageSource = ImageSource.FromFile("default_ingredient.jpg");
            else
                ImageSource = ImageSource.FromFile(FileHelper.GetImageFilePathById(Ingredient.ImageId));

            //Set text displayed based on the context (edit or create)
            if (Ingredient.Id == 0)
            {
                TitleLabel = "Créer un ingrédient";
                ValidationButtonLabel = "Créer";
            }
            else
            {
                TitleLabel = "Modification d'un ingrédient";
                ValidationButtonLabel = "Modifier";
            }

            OpenCategoryListCommand = new Command(async () => await OpenCategoryListPopup());
            PickImageCommand = new Command(async () => await PickImage());
            CreateOrEditIngredientCommand = new Command(async () => await CreateOrEditIngredientAsync());
        }

        public string CategoryLabel
        {
            get
            {
                return _categoryLabel;
            }
            set
            {
                SetValue(ref _categoryLabel, value);
            }
        }

        public ImageSource ImageSource
        {
            get
            {
                return _imageSource;
            }
            set
            {
                SetValue(ref _imageSource, value);
            }
        }

        //Used by categoryListViewModel to update the category
        public void SetCategory(Category category)
        {
            Ingredient.CategoryId = category.Id;
            CategoryLabel = category.Name;
        }


        private async Task OpenCategoryListPopup()
        {
            await PopupNavigation.Instance.PushAsync(new CategoryListPopup(this));
        }

        private async Task PickImage()
        {
            _imageStream = await MediaHelper.UserPickImage();

            //Update the stream if user picked up image
            if (_imageStream != null)
            {
                MemoryStream copyStream = await FileHelper.CopyAndResetPositionStreamAsync(_imageStream);

                ImageSource = ImageSource.FromStream(() =>
                {
                    return copyStream;
                });
            }
        }

        private async Task CreateOrEditIngredientAsync()
        {
            List<Ingredient> list = await DBRequester<Ingredient>.GetItemsAsync();

            if(String.IsNullOrEmpty(Ingredient.Name))
            {
                await PageManager.Instance.DisplayAlert("Erreur", "L'ingrédient n'a pas de nom", "ok");
                return;
            }

            if(Ingredient.CategoryId == 0)
            {
                await PageManager.Instance.DisplayAlert("Erreur", "L'ingrédient n'a pas de catégorie", "ok");
                return;
            }

            //If new Ingredient is created verify if name is already use
            if ((Ingredient.Id != 0 && IngredientViewModel.Name == Ingredient.Name) || (await DBRequester<Ingredient>.GetItemsQueryAsync("SELECT * FROM Ingredient WHERE Name = ?", Ingredient.Name)).Count == 0)
            {
                //Everything is valid proceed to save
                //Save choosed image if not null
                if (_imageStream != null)
                {
                    if (Ingredient.ImageId != 0)
                    {
                        //Remove old image
                        await DBRequester<Models.Image>.DeleteItemAsync(await DBRequester<Models.Image>.GetItemAsync(Ingredient.ImageId));
                        await FileHelper.DeletePngFileFromIdAsync(Ingredient.ImageId);
                    }

                    //Create image
                    await DBRequester<Models.Image>.SaveItemAsync(new Models.Image());
                    Ingredient.ImageId = (await DBRequester<Models.Image>.GetItemsAsync()).Last().Id;

                    await FileHelper.CreatePngFileFromStreamAsync(Ingredient.ImageId, _imageStream);
                }

                await DBRequester<Ingredient>.SaveItemAsync(Ingredient);

                UpdateIngredientsListAction.Invoke(IngredientViewModel, await IngredientViewModel.CreateAsync(Ingredient));
                ReturnCommand.Execute(null);
            }
            else
            {
                await PageManager.Instance.DisplayAlert("Erreur", $"L'ingrédient {Ingredient.Name} existe déjà.", "ok");
            }
        }

        //Return byte array containing the bytes of the given stream
        public static byte[] GetBytesFromStream(Stream fileContentStream)
        {
            using (var memoryStreamHandler = new MemoryStream())
            {
                fileContentStream.CopyTo(memoryStreamHandler);
                return memoryStreamHandler.ToArray();
            }
        }
    }
}
