﻿using Gourmy.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gourmy.ViewModels
{
    public class RecipeIngredientViewModel
    {
        public IngredientViewModel IngredientViewModel { get; set; }

        public RecipeIngredient RecipeIngredient { get; set; }

        public string Name { get; set; }

        public int ImageId { get; set; }

        public int Quantity { get; set; }

        public string QuantityUnit { get; set; }

        public RecipeIngredientViewModel(IngredientViewModel ingredientViewModel, RecipeIngredient recipeIngredient)
        {
            IngredientViewModel = ingredientViewModel;
            RecipeIngredient = recipeIngredient;

            Name = IngredientViewModel.Name;
            ImageId = IngredientViewModel.ImageId;
            Quantity = RecipeIngredient.Quantity;
            QuantityUnit = RecipeIngredient.QuantityUnit;
        }

        public RecipeIngredientViewModel Clone()
        {
            return (RecipeIngredientViewModel)MemberwiseClone();
        }
    }
}
