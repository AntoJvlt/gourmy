﻿using Gourmy.Models;
using Gourmy.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;

namespace Gourmy.ViewModels
{
    class RecipeDisplayViewModel : PageViewModel
    {
        public ICommand EditCommand { get; set; }

        public ICommand DeleteCommand { get; set; }

        public ICommand MakeRecipeCommand { get; set; }

        private Recipe _recipe;

        private ObservableCollection<RecipeIngredientViewModel> _ingredients;

        private ObservableCollection<RecipeStep> _steps;

        private bool _isDeleting = false;

        public RecipeDisplayViewModel(RecipeViewModel recipeViewModel, Action modifiedCallback)
        {
            _recipe = recipeViewModel.Recipe;
            _ingredients = new ObservableCollection<RecipeIngredientViewModel>(recipeViewModel.IngredientsList);
            List<RecipeStep> sortedList = recipeViewModel.StepsList.OrderBy(i => i.Number).ToList();
            _steps = new ObservableCollection<RecipeStep>(sortedList);
            EditCommand = new Command(async () => await Edit(recipeViewModel, modifiedCallback));
            DeleteCommand = new Command(async () => await DeleteAsync(modifiedCallback));
            MakeRecipeCommand = new Command(async () => await OpenCalculationPopupAsync());
        }

        public Recipe Recipe
        {
            get
            {
                return _recipe;
            }
            set
            {
                SetValue<Recipe>(ref _recipe, value);
            }
        }

        public ObservableCollection<RecipeIngredientViewModel> Ingredients
        {
            get
            {
                return _ingredients;
            }
            set
            {
                SetValue<ObservableCollection<RecipeIngredientViewModel>>(ref _ingredients, value);
            }
        }

        public ObservableCollection<RecipeStep> Steps
        {
            get
            {
                return _steps;
            }
            set
            {
                SetValue<ObservableCollection<RecipeStep>>(ref _steps, value);
            }
        }

        private async Task Edit(RecipeViewModel recipeViewModel, Action modifiedCallback)
        {
            if(!_isDeleting)
            {
                await PageManager.Instance.PushAsync(new RecipeEditingPage(recipeViewModel, new Action(async () =>
                {
                    RecipeViewModel newRecipe = await RecipeViewModel.CreateAsync(await DBRequester<Recipe>.GetItemAsync(Recipe.Id));

                    Recipe = newRecipe.Recipe;
                    Ingredients = new ObservableCollection<RecipeIngredientViewModel>(newRecipe.IngredientsList);
                    List<RecipeStep> sortedList = newRecipe.StepsList.OrderBy(i => i.Number).ToList();
                    Steps = new ObservableCollection<RecipeStep>(sortedList);
                    modifiedCallback.Invoke();
                })));
            }
        }

        private async Task DeleteAsync(Action modifiedCallback)
        {
            _isDeleting = true;
            //Ask is user is sure
            if (await PageManager.Instance.DisplayAlert("Attention", $"Voulez-vous vraiment supprimer cette recette ?", "Oui", "Non"))
            {
                //Delete image if set
                if (Recipe.ImageId != 0)
                {
                    Models.Image image = await DBRequester<Models.Image>.GetItemAsync(Recipe.ImageId);
                    await DBRequester<Models.Image>.DeleteItemAsync(image);
                }

                //Delete recipe ingredients
                List<RecipeIngredient> recipeIngredients = await DBRequester<RecipeIngredient>.GetItemsQueryAsync("SELECT * FROM RecipeIngredient WHERE RecipeId = ?", Recipe.Id);

                foreach (RecipeIngredient recipeIngredient in recipeIngredients)
                    await DBRequester<RecipeIngredient>.DeleteItemAsync(recipeIngredient);

                //Delete recipe steps
                List<RecipeStep> recipeSteps = await DBRequester<RecipeStep>.GetItemsQueryAsync("SELECT * FROM RecipeStep WHERE RecipeId = ?", Recipe.Id);

                foreach (RecipeStep recipeStep in recipeSteps)
                    await DBRequester<RecipeStep>.DeleteItemAsync(recipeStep);

                await DBRequester<Recipe>.DeleteItemAsync(Recipe);
                modifiedCallback.Invoke();
                ReturnCommand.Execute(null);
            }
        }

        private async Task OpenCalculationPopupAsync()
        {
            await PopupNavigation.Instance.PushAsync(new CalculationPopup(Ingredients, Recipe.AmountPeople), true);
        }
    }
}
