using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("Brayden.otf", Alias = "Brayden")]
[assembly: ExportFont("Handy.otf", Alias = "Handy")]
[assembly: ExportFont("SilentAsia.otf", Alias = "SilentAsia")]
[assembly: ExportFont("Montserrat.ttf", Alias = "Montserrat")]