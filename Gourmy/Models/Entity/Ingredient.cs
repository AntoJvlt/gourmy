﻿using Gourmy.Helpers;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Gourmy.Models
{
    [Table("Ingredient")]
    public class Ingredient : IDBEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(255), Unique]
        public string Name { get; set; }

        public int CategoryId { get; set; }

        public int ImageId { get; set; }
    }
}
