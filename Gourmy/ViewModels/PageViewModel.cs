﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Gourmy.ViewModels
{
    //ViewModel used by all PageViewModels
    public class PageViewModel : ViewModel
    {
        public ICommand ReturnCommand { get; set; }

        public PageViewModel()
        {
            ReturnCommand = new Command(async () => await ClosePage());
        }

        //Common command used by PageViewModels to close themselves
        private async Task ClosePage()
        {
            await PageManager.Instance.PopAsync();
        }
    }
}
